#ifndef MVC_HASHTABLE_H
# define MVC_HASHTABLE_H

# include <stddef.h>

# ifndef _BSD_SOURCE
#  define _BSD_SOURCE
# endif // !_BSD_SOURCE

typedef struct htab s_htab;
typedef struct helt s_helt;

struct htab
{
    s_helt **elt;
    size_t size;
};

struct helt
{
    char *key;
    void *data;
    s_helt *next;
};

s_htab *htab_init(size_t size);
void htab_set(s_htab *tab, const char *key, void **data);
void *htab_get(const s_htab *tab, const char *key);
void htab_destroy(s_htab *tab);

#endif // !MVC_HASHTABLE_H
