#include "views.h"
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <assert.h>

static size_t parse_view(s_view *view, char *s);

static s_view *init_view(void)
{
    s_view *view = malloc(sizeof (s_view));
    view->chunk = NULL;
    view->size = 0;
    view->vars = htab_init(HTAB_VARS_SIZE);
    view->sections = htab_init(HTAB_SECTIONS_SIZE);
    view->data_ptr = NULL;
    return view;
}

static void save_chunk(s_view *view, e_view type, char *name,
                       s_view *section, char *data)
{
    if (data != NULL && data[0] == '\0')
        return;

    ++view->size;
    if ((view->size - 1) % CHUNKS_ALLOC_STEP == 0)
    {
        view->chunk = realloc(view->chunk,
            ((view->size / CHUNKS_ALLOC_STEP) + 1) * CHUNKS_ALLOC_STEP *
                sizeof (s_chunk *));
    }

    s_chunk *chunk = malloc(sizeof (s_chunk));
    chunk->type = type;
    chunk->name = name;
    chunk->section = section;
    chunk->section_instances = section == NULL ? NULL : init_view();
    chunk->data = data;

    view->chunk[view->size - 1] = chunk;
}

static size_t parse_var(s_view *view, char *s)
{
    size_t pos = 0;
    size_t start = 0;
    for (; s[pos] != '$' && s[pos] != '\0'; ++pos);
    start = pos + 1;
    for (; s[pos] != ' ' && s[pos] != '\t' && s[pos] != '\n' &&
           s[pos] != '\0' && s[pos] != '}'; ++pos);
    size_t end = pos;
    for (; s[pos] == ' ' || s[pos] == '\t' || s[pos] == '\n' ||
           s[pos] == '\0'; ++pos);
    if (s[pos] != '}')
        return 0;

    s[end] = '\0';
    save_chunk(view, V_VAR, s + start, NULL, NULL);
    htab_set(view->vars, s + start, (void **)&view->chunk[view->size - 1]);

    return pos + 1;
}

static size_t parse_section(s_view *view, char *s)
{
    size_t pos = 0;
    size_t start = 0;
    for (; s[pos] != '&' && s[pos] != '\0'; ++pos);
    start = pos + 1;
    for (; s[pos] != ' ' && s[pos] != '\t' && s[pos] != '\n' &&
           s[pos] != '\0' && s[pos] != '}'; ++pos);
    size_t end = pos;
    for (; s[pos] == ' ' || s[pos] == '\t' || s[pos] == '\n' ||
           s[pos] == '\0'; ++pos);
    if (s[pos] != '}')
        return 0;

    s[end] = '\0';
    s_view *section = init_view();
    save_chunk(view, V_SECTION, s + start, section, NULL);
    htab_set(view->sections, s + start, (void **)&view->chunk[view->size - 1]);

    return parse_view(section, s + pos + 1) + pos + 1;
}

static char *get_brack_id(char *s)
{
    size_t i;
    for (i = 0; s[i] == ' ' || s[i] == '\t' || s[i] == '\n'; ++i);
    if (s[i] == '$' || s[i] == '&')
        return s + i;
    return s;
}

static size_t parse_view(s_view *view, char *s)
{
    size_t pos = 0;
    size_t i = 0;
    for (; s[i] != '\0'; ++i)
    {
        if (s[i] == '{')
        {
            char *id = get_brack_id(s + i + 1);
            if (id == '\0')
                continue;

            size_t shift = 0;
            if (*id == '&')
            {
                if (id[1] == '!')
                {
                    s[i++] = '\0';
                    for (; s[i] != '}' && s[i] != '\0'; ++i);
                    ++i;
                    break;
                }
                else
                    shift = parse_section(view, s + i + 1);
            }
            else if (*id == '$')
                shift = parse_var(view, s + i + 1);

            if (shift > 0)
            {
                s[i] = '\0';
                save_chunk(view, V_TEXT, NULL, NULL, s + pos);
                if (view->size >= 2)
                {
                    s_chunk *tmp = view->chunk[view->size - 1];
                    view->chunk[view->size - 1] = view->chunk[view->size - 2];
                    view->chunk[view->size - 2] = tmp;
                }

                i += shift;
                if (s[i + 1] != '\0')
                    ++i;
                pos = i;
            }
        }
    }
    save_chunk(view, V_TEXT, NULL, NULL, s + pos);
    return i;
}

s_view *view_get(const char *name)
{
    FILE *fp = fopen(name, "r"); // TODO: handle path root/%s.html
    if (fp == NULL)
        return NULL;

    s_view *view = init_view();

    char *buf = malloc(VIEW_READ_STEP);
    size_t alloc = VIEW_READ_STEP;
    size_t pos = 0;
    size_t len;

    while ((len = fread(buf + pos, 1, VIEW_READ_STEP, fp)) > 0)
    {
        pos += len;
        if (pos + VIEW_READ_STEP > alloc)
        {
            alloc += VIEW_READ_STEP;
            buf = realloc(buf, alloc);
        }
    }
    buf[pos] = '\0';

    view->data_ptr = buf;
    parse_view(view, view->data_ptr);

    fclose(fp);
    return view;
}

void view_set_var(const s_view *view, const char *name, const char *value)
{
    assert(view != NULL);
    s_chunk *chunk = htab_get(view->vars, name);
    while (chunk != NULL)
    {
        if (chunk->data != NULL)
            free(chunk->data);
        chunk->data = strdup(value);
        chunk = htab_get(NULL, name);
    }
}

s_chunk *view_get_section(const s_view *view, const char *name)
{
    return htab_get(view->sections, name);
}

void section_set_var(const s_chunk *chunk, const char *name, const char *value)
{
    assert(chunk != NULL);
    view_set_var(chunk->section, name, value);
}

void section_save_instance(const s_chunk *chunk)
{
    assert(chunk != NULL);
    s_view *dest = chunk->section_instances;
    for (size_t i = 0; i < chunk->section->size; ++i)
    {
        s_chunk *src = chunk->section->chunk[i];
        if (src->type == V_TEXT)
            save_chunk(dest, V_TEXT, src->name, NULL, src->data);
        else if (src->type == V_VAR)
            save_chunk(dest, V_VAR, src->name, NULL, strdup(src->data));
        // TODO: handle sections
    }
}

void view_display(s_view *view, FILE *dest)
{
    assert(view != NULL);
    for (size_t i = 0; i < view->size; ++i)
    {
        s_chunk *chunk = view->chunk[i];
        if (chunk->type == V_SECTION)
        {
            view_display(chunk->section_instances, dest);
            view_display(chunk->section, NULL);
        }
        else if (chunk->data != NULL)
        {
            if (dest != NULL)
                fprintf(dest, "%s", chunk->data);
            if (chunk->type == V_VAR)
                free(chunk->data);
        }
        free(chunk);
    }
    free(view->chunk);
    if (dest != NULL)
        fflush(dest);

    htab_destroy(view->vars);
    htab_destroy(view->sections);
    if (view->data_ptr != NULL) // section->data_ptr is null
        free(view->data_ptr);
    free(view);
}
