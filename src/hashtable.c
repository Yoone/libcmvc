#include "hashtable.h"
#include <stdlib.h>
#include <string.h>

static s_helt *g_last_elt = NULL;

s_htab *htab_init(size_t size)
{
    s_htab *tab = malloc(sizeof (s_htab));
    tab->elt = malloc(size * sizeof (s_helt));
    tab->size = size;

    for (size_t i = 0; i < tab->size; ++i)
        tab->elt[i] = NULL;

    return tab;
}

static size_t hash(const char *key, size_t size)
{
    size_t res = 0;
    for (size_t i = 0; key[i] != '\0'; ++i)
        res += key[i];

    return res % size;
}

static s_helt *elt_init(const char *key, void **data)
{
    s_helt *elt = malloc(sizeof (s_helt));
    elt->key = strdup(key);
    elt->data = *data;
    elt->next = NULL;
    return elt;
}

void htab_set(s_htab *tab, const char *key, void **data)
{
    size_t h = hash(key, tab->size);

    if (tab->elt[h] != NULL)
    {
        s_helt *elt = tab->elt[h];
        while (elt->next != NULL)
        {
            if (strcmp(elt->key, key) == 0)
                return;
            elt = elt->next;
        }
        elt->next = elt_init(key, data);
    }
    else
        tab->elt[h] = elt_init(key, data);
}

void *htab_get(const s_htab *tab, const char *key)
{
    s_helt *elt;
    if (tab != NULL)
    {
        size_t h = hash(key, tab->size);
        elt = tab->elt[h];
    }
    else
        elt = g_last_elt;

    while (elt != NULL)
    {
        g_last_elt = elt->next;
        if (strcmp(elt->key, key) == 0)
            return elt->data;
        elt = elt->next;
    }

    return NULL;
}

void htab_destroy(s_htab *tab)
{
    for (size_t i = 0; i < tab->size; ++i)
    {
        s_helt *elt = tab->elt[i];
        while (elt != NULL)
        {
            free(elt->key);
            s_helt *tmp = elt->next;
            free(elt);
            elt = tmp;
        }
    }
    free(tab->elt);
    free(tab);
}
