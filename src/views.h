#ifndef MVC_VIEWS_H
# define MVC_VIEWS_H

# define CHUNKS_ALLOC_STEP      (10)
# define VIEW_READ_STEP         (4096)
# define HTAB_VARS_SIZE         (64)
# define HTAB_SECTIONS_SIZE     (32)

# ifndef _BSD_SOURCE
#  define _BSD_SOURCE
# endif // !BSD_SOURCE

# include "hashtable.h"
# include <stdio.h>

typedef struct view s_view;
typedef struct chunk s_chunk;

struct view
{
    s_chunk **chunk;
    size_t size; // Number of chunks
    s_htab *vars;
    s_htab *sections;
    char *data_ptr; // Index of parsed data, used to free
};

typedef enum
{
    V_TEXT,
    V_VAR,
    V_SECTION
} e_view;

struct chunk
{
    e_view type;
    char *name; // NULL if type == V_TEXT
    s_view *section; // NULL if type != V_SECTION
    s_view *section_instances; // NULL if type != V_SECTION
    char *data; // NULL until generated if type != V_TEXT
};

s_view *view_get(const char *name);
void view_set_var(const s_view *view, const char *name, const char *value);
s_chunk *view_get_section(const s_view *view, const char *name);
void section_set_var(const s_chunk *chunk, const char *name, const char *value);
void section_save_instance(const s_chunk *chunk);
void view_display(s_view *view, FILE *dest);

#endif // !MVC_VIEWS_H
