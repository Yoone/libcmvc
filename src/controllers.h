#ifndef MVC_CONTROLLERS_H
# define MVC_CONTROLLERS_H

# include <stdlib.h>

typedef struct
{
    const char *name;
    int (*callback)(void); // TODO: update prototype
} s_controller_meta;

# define CONTROLLER(Name)                               \
    int ctrl_##Name(void);                              \
    __attribute__ ((section("controllers"), unused))    \
    static s_controller_meta ctrl_##Name##_meta =       \
    {                                                   \
        .name = #Name,                                  \
        .callback = &ctrl_##Name                        \
    };                                                  \
    int ctrl_##Name(void)

# define SET_ERROR_CONTROLLER(Name) g_error_ctrl = &ctrl_##Name##_meta

# define CALL_CONTROLLER(Name) ctrl_##Name##_meta.callback()

int controller_exec(const char *name);
extern s_controller_meta *g_error_ctrl;

#endif // !MVC_CONTROLLERS_H
