#include "controllers.h"
#include <string.h>

extern s_controller_meta __start_controllers;
extern s_controller_meta __stop_controllers;

s_controller_meta *g_error_ctrl = NULL;

CONTROLLER(default_error)
{
    return 500;
}

int controller_exec(const char *name)
{
    s_controller_meta *const ctrl_start = &__start_controllers;
    s_controller_meta *const ctrl_end = &__stop_controllers;

    for (s_controller_meta *m = ctrl_start; m < ctrl_end; ++m)
        if (strcmp(m->name, name) == 0)
            return m->callback(); // TODO: update prototype

    if (g_error_ctrl != NULL)
        return g_error_ctrl->callback(); // TODO: update prototype

    return CALL_CONTROLLER(default_error);
}
