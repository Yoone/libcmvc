#include "controllers.h"
#include "views.h"
#include <stdio.h>

CONTROLLER(home)
{
    printf("This is home\n");
    return 200;
}

CONTROLLER(contact)
{
    printf("Contact page!\n");
    return 200;
}

CONTROLLER(404)
{
    printf("Error 404: page not found.\n");
    return 404;
}

int main(void)
{
    SET_ERROR_CONTROLLER(404);
    //printf("HTTP code: %d\n", controller_exec("home"));
    s_view *view = view_get("test_leaks.html");
    view_set_var(view, "title", "My website");
    view_set_var(view, "username", "lol");
    view_set_var(view, "username", "Yoone"); // no leaks
    view_set_var(view, "test", "Testing var");

    s_chunk *section = view_get_section(view, "users");
    for (size_t i = 1; i <= 2; ++i)
    {
        char *id = malloc(16);
        sprintf(id, "%u", i);
        section_set_var(section, "id", id);
        free(id);
        section_set_var(section, "name", "TestUser");
        section_save_instance(section);
    }

    view_display(view, stdout);
    return 0;
}
