CC = gcc
CFLAGS = -std=c99 -Wall -Werror -Wextra -pedantic
LDLIBS =
DEBUG = -g3 -D DEBUG
PROD = -O2
SOUT = libcmvc.a
DOUT = libcmvc.so
DVER = 1

SRC = controllers.c                     \
      views.c                           \
      hashtable.c                       \

OBJ := $(SRC:.c=.o)

SRCDIR = src/
BLDDIR = build/
SRC := $(addprefix $(SRCDIR), $(SRC))
OBJ := $(addprefix $(BLDDIR), $(OBJ))

.PHONY: clean distclean

all: CFLAGS += $(PROD)
all: $(SOUT) $(DOUT)

$(SOUT): $(OBJ)
	ar -cvq $@ $^ $(LDLIBS)

$(DOUT): CFLAGS += -fPIC
$(DOUT): $(OBJ)
	$(CC) -shared -Wl,-soname,$@.$(DVER) -o $@ $^ $(LDLIBS)

$(BLDDIR):
	mkdir -p $(BLDDIR)

$(BLDDIR)%.o: $(SRCDIR)%.c | $(BLDDIR)
	$(CC) $(CFLAGS) -c $^ -o $@

debug: CFLAGS += $(DEBUG)
debug: $(OBJ) $(BLDDIR)main.o
	$(CC) -o $@ $^ $(LDLIBS)

clean:
	$(RM) $(OBJ)
	$(RM) $(SOUT)
	$(RM) $(DOUT)
	$(RM) debug

distclean: clean
	$(RM) -r $(BLDDIR)
